package at.rhiki.shoppinglist.ui

import android.animation.ValueAnimator
import android.content.Context
import android.view.View
import androidx.annotation.ColorRes
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import at.rhiki.shoppinglist.R
import at.rhiki.shoppinglist.ui.shoppinglist.ShoppingLingListAdapter
import at.rhiki.shoppinglist.ui.shoppinglist.ShoppingListViewModel

/**
 * Helper for handling ItemTouchHelper callbacks to support swiping and dragging items.
 * @property context required to resolve color resource IDs to actual colors.
 * @property adapter the RecyclerView's adapter this ItemTouchHelper operates on
 * @property viewModel the ViewModel, which acts as a data source, so swipe-deleted items can be removed there as well.
 */
class ShoppingListRecyclerTouchHelper(
    private val context: Context,
    private val adapter: ShoppingLingListAdapter,
    private val viewModel: ShoppingListViewModel
) : ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or ItemTouchHelper.DOWN, ItemTouchHelper.LEFT) {

    companion object {
        /**
         * The background color of items that are to be deleted, by swiping them either to left or the right.
         * @see [R.color.shopping_list_swipe_to_delete_red]
         */
        @ColorRes
        const val BACKGROUND_DELETE = R.color.shopping_list_swipe_to_delete_red

        /**
         * The background color of items that are swiped back into their normal position
         * @see [R.color.shopping_list_swipe_to_delete_red]
         */
        @ColorRes
        const val BACKGROUND_NORMAL = R.color.colorPrimary

        /**
         * Duration, in milliseconds, it takes to fully blend from the starting color to ending color.
         */
        const val ANIMATION_DURATION: Long = 500

        /**
         * Set the threshold at which a swipe action is considered completed.
         */
        const val SWIPE_THRESHOLD: Float = 0.8f
    }

    /**
     * Store the last swiped view to be able to restore it's original background color when swiping is cancelled
     */
    private var lastSwipedView: View? = null

    /**
     * Move the items in the [adapter] as the items in the view are moved.
     */
    override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
        this.adapter.moveItem(viewHolder.absoluteAdapterPosition, target.absoluteAdapterPosition)
        return true
    }

    /**
     * Delete the swiped item from the [adapter] and [viewModel].
     * @param viewHolder the swiped item
     * @param direction swipe direction - not used as either direction results in a swipe to delete
     */
    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        this.adapter.itemAtPosition(viewHolder.bindingAdapterPosition)?.let { this.viewModel.deleteItem(it) }
        this.adapter.notifyItemRemoved(viewHolder.bindingAdapterPosition)
    }

    /**
     * Called whenever an item get moved/swiped around or returns to it's resting position.
     * When the performed action is a swipe we store the targeted [viewHolder] in [lastSwipedView], so the background color can be reverted
     * in [clearView].
     */
    override fun onSelectedChanged(viewHolder: RecyclerView.ViewHolder?, actionState: Int) {
        super.onSelectedChanged(viewHolder, actionState)
        if (actionState == ItemTouchHelper.ACTION_STATE_SWIPE) {
            this.lastSwipedView = viewHolder?.itemView
            this.lastSwipedView?.let { this.animateBackground(it, BACKGROUND_NORMAL, BACKGROUND_DELETE) }
        }
    }

    /**
     * Revert the background of swiped items, if there was a swiped item and reset [lastSwipedView].
     */
    override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
        super.clearView(recyclerView, viewHolder)
        this.lastSwipedView?.let { this.animateBackground(it, BACKGROUND_DELETE, BACKGROUND_NORMAL) }
        this.lastSwipedView = null
    }

    override fun getSwipeThreshold(viewHolder: RecyclerView.ViewHolder): Float = SWIPE_THRESHOLD


    /**
     * Creates and starts a [ValueAnimator] that changes the [target]'s [View.setBackgroundColor] from the [from] color resource
     * to the [to] color resource over the span of [ANIMATION_DURATION] milliseconds.
     * @param target the view which' background color is to be animated
     * @param from Android color resource to be used as the starting point of the animation
     * @param to Android color resource to be used as the ending point of the animation
     */
    private fun animateBackground(target: View, @ColorRes from: Int, @ColorRes to: Int) {
        val animator = ValueAnimator.ofArgb(this.context.getColor(from), this.context.getColor(to))
        animator.duration = ANIMATION_DURATION
        animator.addUpdateListener { target.setBackgroundColor(animator.getAnimatedValue() as Int) }
        animator.start()
    }
}