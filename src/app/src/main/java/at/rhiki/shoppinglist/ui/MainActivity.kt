package at.rhiki.shoppinglist.ui

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import at.rhiki.shoppinglist.R
import at.rhiki.shoppinglist.ui.shoppinglist.ShoppingListFragment

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main_activity)
        if (savedInstanceState == null) {
            this.supportFragmentManager.beginTransaction()
                .replace(R.id.container, ShoppingListFragment.newInstance())
                .commitNow()
        }
    }

    override fun onStop() {
        super.onStop()
        viewModelStore.clear()
    }
}
