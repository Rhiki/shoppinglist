package at.rhiki.shoppinglist.model

import androidx.lifecycle.LiveData

class ShoppingListRepository(private val strategy: ShoppingListRepositoryStrategy) {

    private val allItems = this.strategy.getAll()

    /**
     * Returns a [LiveData] object containing all [ShoppingListItem]s
     */
    fun getAll(): LiveData<List<ShoppingListItem>> = this.allItems

    /**
     * Insert the passed [items] into the underlying data source. This is executed asynchronously.
     */
    fun insert(vararg items: ShoppingListItem) = Thread { strategy.insert(*items) }.start()

    /**
     * Deletes the passed [items] from the underlying data source. This is executed asynchronously.
     */
    fun delete(vararg items: ShoppingListItem) = Thread { strategy.delete(*items) }.start()
    /**
     * Updates the passed [items] in the underlying data source. This is executed asynchronously.
     */
    fun update(vararg items: ShoppingListItem) = Thread { strategy.update(*items) }.start()
    /**
     * Updates the passed [items] in the underlying data source, if they already exist.
     * Otherwise they are inserted into the underlying data source. This is executed asynchronously.
     */
    fun updateOrInsert(vararg items: ShoppingListItem) = Thread { strategy.updateOrInsert(*items) }.start();


}

interface ShoppingListRepositoryStrategy {
    /**
     * Retrieves all [ShoppingListItem]s from the data source.
     */
    fun getAll(): LiveData<List<ShoppingListItem>>

    /**
     * Insert the given items into the data source.
     */
    fun insert(vararg items: ShoppingListItem)

    /**
     * Delete the given items from the data source.
     */
    fun delete(vararg items: ShoppingListItem)

    /**
     * Update the given items in the data source.
     */
    fun update(vararg items: ShoppingListItem)

    /**
     * Update items if they are already in the data source, or insert them if they aren't yet.
     */
    fun updateOrInsert(vararg items: ShoppingListItem)
}