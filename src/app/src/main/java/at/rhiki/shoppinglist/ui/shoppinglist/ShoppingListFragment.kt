package at.rhiki.shoppinglist.ui.shoppinglist

import android.app.AlertDialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import at.rhiki.shoppinglist.R
import at.rhiki.shoppinglist.databinding.DialogShoppingListItemBinding
import at.rhiki.shoppinglist.databinding.ShoppingListFragmentBinding
import at.rhiki.shoppinglist.databinding.ShoppingListItemBinding
import at.rhiki.shoppinglist.model.ShoppingListItem
import at.rhiki.shoppinglist.ui.ShoppingListRecyclerTouchHelper

class ShoppingListFragment : Fragment() {

    companion object {
        fun newInstance() = ShoppingListFragment()
    }

    private val adapter: ShoppingLingListAdapter by lazy {
        ShoppingLingListAdapter(this.requireContext(), this.viewModel, onLongClick = { this.showEditItemDialog(it) })
    }
    private val viewModel: ShoppingListViewModel by lazy { ViewModelProvider(this)[ShoppingListViewModel::class.java] }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        val binding = ShoppingListFragmentBinding.inflate(inflater, container, false)
        binding.viewModel = this.viewModel
        this.initRecyclerView(binding.recyclerShoppingList)
        this.hookUpObservers()
        return binding.root
    }

    private fun initRecyclerView(recyclerView: RecyclerView) {
        recyclerView.adapter = this.adapter
        recyclerView.layoutManager = LinearLayoutManager(this.context)
        ItemTouchHelper(ShoppingListRecyclerTouchHelper(this.requireContext(), this.adapter, this.viewModel)).attachToRecyclerView(
            recyclerView
        )
    }

    private fun hookUpObservers() {
        this.viewModel.createNewItem.obs(this.viewLifecycleOwner, Observer { if (it == true) this.showNewItemCreationDialog() })
        this.viewModel.updateShoppingList.obs(this.viewLifecycleOwner, Observer { if (it == true) this.adapter.notifyDataSetChanged() })
        this.viewModel.editItem.obs(this.viewLifecycleOwner, Observer { it?.let { this.showEditItemDialog(it) } })
    }

    private fun showNewItemCreationDialog() {
        this.showShoppingListItemDialog(ShoppingListItem(name = "")) { this.adapter.notifyItemInserted(this.viewModel.addItem(it)) }
    }

    private fun showEditItemDialog(item: ShoppingListItem) {
        this.showShoppingListItemDialog(item) { this.adapter.notifyItemChanged(item) }
    }

    private fun showShoppingListItemDialog(shoppingListItem: ShoppingListItem, onConfirm: (item: ShoppingListItem) -> Unit) {
        val binding = DialogShoppingListItemBinding.inflate(LayoutInflater.from(this.context))
        binding.item = shoppingListItem
        binding.useDescription = shoppingListItem.details.isNotEmpty()
        binding.txtItemName.requestFocus()

        AlertDialog.Builder(this.context, R.style.DialogTheme)
            .setTitle(if (shoppingListItem.id == 0L) R.string.dialog_sil_title_create else R.string.dialog_sil_title_edit)
            .setView(binding.root)
            .setCancelable(true)
            .setPositiveButton(R.string.dialog_sil_save) { _: DialogInterface, _: Int -> onConfirm(shoppingListItem) }
            .show()
    }
}

/**
 * List adapter to display [ShoppingListItem].
 * @param context required to instantiate a [LayoutInflater]
 * @param viewModel The view model to retrieve the shopping list from
 * @param onLongClick callback for when an item in the list received a long click event
 */
class ShoppingLingListAdapter(
    context: Context, private val viewModel: ShoppingListViewModel,
    private val onLongClick: (item: ShoppingListItem) -> Unit = {}
) :
    RecyclerView.Adapter<ShoppingListViewHolder>() {
    private val inflater: LayoutInflater = LayoutInflater.from(context)
    private val data: MutableList<ShoppingListItem> = viewModel.shoppingList

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShoppingListViewHolder {
        val binding = ShoppingListItemBinding.inflate(this.inflater, parent, false)
        return ShoppingListViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ShoppingListViewHolder, position: Int) {
        val item = this.data[position]
        holder.binding.item = item
        holder.binding.lblShoppingListItem.setOnClickListener {
            this.viewModel.toggleItem(item)
            this.notifyItemChanged(position)
        }
        holder.binding.btnEdit.setOnClickListener { this.viewModel.onEditClick(item) }
    }

    override fun getItemCount(): Int = this.data.size

    fun itemAtPosition(position: Int) = if (position >= 0 && position < this.data.size) this.data[position] else null

    fun notifyItemChanged(item: ShoppingListItem) {
        val position = this.data.indexOf(item)
        if (position >= 0) {
            this.notifyItemChanged(position)
        }
    }

    fun moveItem(fromPosition: Int, toPosition: Int) {
        this.data.add(toPosition, this.data.removeAt(fromPosition))
        this.notifyItemMoved(fromPosition, toPosition)
    }
}

class ShoppingListViewHolder(val binding: ShoppingListItemBinding) : RecyclerView.ViewHolder(binding.root)
