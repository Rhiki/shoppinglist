package at.rhiki.shoppinglist

import android.app.Application
import at.rhiki.shoppinglist.model.AppDatabase

class RslApplication : Application() {

    override fun onCreate() {
        super.onCreate()
        AppDatabase.init(this.applicationContext)
    }
}