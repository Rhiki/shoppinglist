package at.rhiki.shoppinglist.ui.shoppinglist

import android.util.Log
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModel
import at.rhiki.shoppinglist.ConsumableLiveData
import at.rhiki.shoppinglist.model.ShoppingListItem
import at.rhiki.shoppinglist.model.ShoppingListRepository
import at.rhiki.shoppinglist.model.ShoppingListStrategyRoomDb

class ShoppingListViewModel : ViewModel() {

    private val repo: ShoppingListRepository = ShoppingListRepository(ShoppingListStrategyRoomDb()) // TODO: dependency injection

    private val toDelete = mutableListOf<ShoppingListItem>()

    private val observer = Observer<List<ShoppingListItem>> {
        this.shoppingList.clear()
        this.shoppingList.addAll(it)
        this.toDelete.clear()
        this.updateShoppingList.post(true)
    }

    val shoppingList: MutableList<ShoppingListItem> by lazy {
        this.repo.getAll().observeForever(this.observer)
        return@lazy this.repo.getAll().value?.toMutableList() ?: mutableListOf()
    }
    val createNewItem: ConsumableLiveData<Boolean> = ConsumableLiveData(false)
    val updateShoppingList: ConsumableLiveData<Boolean> = ConsumableLiveData(false)
    val editItem: ConsumableLiveData<ShoppingListItem> = ConsumableLiveData(null)

    override fun onCleared() {
        super.onCleared()
        this.repo.getAll().removeObserver(this.observer)
        this.persistShoppingList()
    }

    fun toggleItem(item: ShoppingListItem) {
        item.completed = !item.completed
    }

    fun onCreateNewItemClick() = this.createNewItem.post(true)

    fun addItem(item: ShoppingListItem): Int {
        this.shoppingList.add(item)
        item.order = this.shoppingList.indexOf(item)
        return item.order
    }

    fun deleteItem(item: ShoppingListItem) {
        this.shoppingList.remove(item)
        this.toDelete.add(item)
    }

    fun onEditClick(item: ShoppingListItem) = this.editItem.post(item)

    private fun persistShoppingList() {
        this.recalculateItemPositions()
        this.repo.updateOrInsert(*this.shoppingList.toTypedArray())
        this.repo.delete(*this.toDelete.toTypedArray())
    }

    private fun recalculateItemPositions() {
        this.shoppingList.forEachIndexed { index, item -> item.order = index }
    }
}
