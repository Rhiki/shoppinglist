package at.rhiki.shoppinglist

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Observer

/**
 * A wrapper to assist with Android's LiveData to allow an object to be read only a set amount of times without needing to manage the value
 * from the receiving observer. Allows peeking the value unlimited amount of times to support (for example) logging.
 * @param T type of the wrapped value
 */
interface Consumable<out T> {
    /**
     * Consumes the wrapped value.
     * @return the wrapped value if it was not fully consumed yet; null if the wrapped value was already consumed.
     */
    fun consume(): T?

    /**
     * Peek into the wrapped value. Allows accessing the wrapped value without consuming it, or accessing it after it was already consumed.
     * Should not be used for normally accessing it, but may be used for use cases like logging.
     * @return the wrapped value.
     */
    fun peek(): T
}

/**
 * Implementation of [Consumable], that allows the wrapped value to only be consumed once.
 * Example usage would be for transmitting events that should only trigger their action once with Android's LiveData.
 * @param T type of the wrapped value
 * @property content the value to wrap
 * @constructor creates a Consumable wrapper for the passed value
 */
open class ConsumableOnce<out T>(private val content: T) : Consumable<T> {
    private var consumed = false

    /**
     * Consume this wrapper's value. This method will return the real value only _once_, any additional calls to this method will
     * receive _null_ as return value.
     * @return the wrapped value, if it was not consumed yet; otherwise _null_
     */
    override fun consume(): T? {
        if (this.consumed) {
            return null
        } else {
            this.consumed = true
            return this.content
        }
    }

    override fun peek(): T = content
}

/**
 * Implementation of [Consumable] that allows to set the amount of times the wrapped value may be consumed.
 * @param T type of the wrapped value
 * @property content the wrapped value
 * @property maxConsume the amount of times the wrapped value may be consumed
 * @constructor Creates a wrapper for the [content], that may be accessed [maxConsume] amount of times.
 */
open class ConsumableNTimes<out T>(private val content: T, private val maxConsume: Int) : Consumable<T> {
    private var consumeCount = 0;

    /**
     * Consume this wrapper's value if the maximum amount of consumptions wasn't used up yet.
     * Increases the internal consume counter
     * @return the wrapped value, if it wasn't fully consumed yet; null otherwise
     */
    override fun consume(): T? = if (this.consumeCount++ <= this.maxConsume) this.content else null

    override fun peek(): T = content
}

/**
 * A wrapper for to make working with [MutableLiveData] and [Consumable] easier.
 *
 * By default the implementation of [ConsumableOnce] will be used, but you can override how Consumables should be created by
 * setting the [creator] parameter.
 * For posting/setting/getting you should use the following methods:
 * - [ConsumableLiveData.get]
 * - [ConsumableLiveData.set]
 * - [ConsumableLiveData.post]
 * - [ConsumableLiveData.obs]
 *
 * Using [MutableLiveData]'s normal postValue, setValue and getValue works, but
 * it defeats the purpose of this wrapper.
 * @param T type of value to be stored
 */
open class ConsumableLiveData<T>(
    value: T? = null, private val creator: (value: T?) -> Consumable<T?> = { ConsumableOnce(it) }
) : MutableLiveData<Consumable<T?>>(ConsumableOnce(value)) {

    private fun create(value: T?): Consumable<T?> {
        return this.creator.invoke(value)
    }

    /**
     * Post a value from a background to this LiveData object.
     * @param value the value to post.
     * @see [MutableLiveData.postValue]
     */
    fun post(value: T?) = this.postValue(this.create(value))

    /**
     * Set a value from the main thread to this LiveData object.
     * @param value the value to post.
     * @see [MutableLiveData.setValue]
     */
    fun set(value: T?) = super.setValue(this.create(value))

    /**
     * Get this LiveData's value
     * @param value the value to post.
     * @see [MutableLiveData.getValue]
     */
    fun get(value: T?) = super.getValue()?.consume()

    /**
     * Observe the LiveData's value.
     * @see [MutableLiveData.observe]
     */
    fun obs(owner: LifecycleOwner, observer: Observer<in T?>) {
        super.observe(owner, Observer { observer.onChanged(it.consume()) })
    }

}