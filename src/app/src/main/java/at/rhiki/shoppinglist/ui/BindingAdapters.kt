package at.rhiki.shoppinglist.ui

import android.graphics.Paint
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.databinding.BindingAdapter
import androidx.databinding.InverseBindingAdapter

/**
 * Android BindingAdapter to set _'app:strikeThrough'_ on an EditText in XML/Databinding to make it's text content appear with a
 * strike through decoration.
 * @param view The EditText to apply the value on
 * @param strikeThrough true to apply a strike through text decoration; false to disable a strike-through text decoration
 */
@BindingAdapter("app:strikeThrough")
fun strikeThrough(view: EditText, strikeThrough: Boolean) {
    if (strikeThrough) {
        view.paintFlags = view.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
    } else {
        view.paintFlags = view.paintFlags and Paint.STRIKE_THRU_TEXT_FLAG.inv()
    }
}

/**
 * Android BindingAdapter to apply an Int value directly to a TextView without having to cast or wrap it somehow in XML/Databinding.
 * @param view the TextView to apply the value to
 * @param amount the Int value to be set as text
 */
@BindingAdapter("app:amount")
fun setAmount(view: TextView, amount: Int) {
    view.text = amount.toString()
}

// TODO
@InverseBindingAdapter(attribute = "app:amount")
fun getAmount(view: TextView): Int = (view.text as String).toIntOrNull() ?: 0

/**
 * Android BindingAdapter to set a View's visibility with a Boolean value in XML/Databinding.
 * @param view the targeted view object
 * @param visible true to show the view; false to hide the view
 */
@BindingAdapter("android:visibility")
fun booleanToVisibility(view: View, visible: Boolean) {
    view.visibility = if (visible) View.VISIBLE else View.GONE
}


