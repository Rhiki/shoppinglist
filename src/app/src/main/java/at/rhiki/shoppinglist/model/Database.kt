package at.rhiki.shoppinglist.model

import android.content.Context
import android.util.Log
import androidx.lifecycle.LiveData
import androidx.room.*
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(version = 4, entities = [ShoppingListItem::class], exportSchema = true)
abstract class AppDatabase : RoomDatabase() {
    abstract fun shoppingListDao(): ShoppingListDao

    companion object {
        lateinit var instance: AppDatabase

        fun init(context: Context) {
            instance = Room.databaseBuilder(context, AppDatabase::class.java, "shopping-list-items.db")
                .addMigrations(*migrations())
                .build()
        }

        private fun migrations(): Array<Migration> {
            return arrayOf(
                NoOpMigration(1, 2),
                MigrationImpl(2, 3)
                { database -> database.execSQL("ALTER TABLE ShoppingListItem ADD COLUMN deleted INTEGER DEFAULT 0 NOT NULL") },
                MigrationImpl(3, 4)
                { database -> database.execSQL("ALTER TABLE ShoppingListItem ADD COLUMN `order` INTEGER DEFAULT 0 NOT NULL") }
            )
        }
    }
}

@Dao
interface ShoppingListDao {
    @Query("SELECT * FROM  ShoppingListItem ORDER BY `order` ASC")
    fun all(): LiveData<List<ShoppingListItem>>

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun insert(vararg items: ShoppingListItem)

    @Delete
    fun delete(vararg items: ShoppingListItem)

    @Update
    fun update(vararg items: ShoppingListItem)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun updateOrInsert(vararg items: ShoppingListItem)
}

@Entity(tableName = "ShoppingListItem")
data class ShoppingListItem(
    @PrimaryKey(autoGenerate = true) val id: Long = 0,
    @ColumnInfo(name = "name") var name: String = "",
    @ColumnInfo(name = "quantity") var quantity: Int = 1,
    @ColumnInfo(name = "completed") var completed: Boolean = false,
    @ColumnInfo(name = "details") var details: String = "",
    @ColumnInfo(name = "order") var order: Int = Int.MAX_VALUE
) {
    fun toDisplayString() = if (this.quantity > 1) "${this.quantity}x ${this.name}" else this.name

    /**
     * Convenience getter to use [quantity] with Android's Databinding on TextViews
     */
    fun getQuantityString(): String = this.quantity.toString()

    /**
     * Convenience setter to use [quantity] with Android's Databinding on TextViews
     */
    fun setQuantityString(value: String) {
        this.quantity = value.toIntOrNull() ?: 1
    }
}

class ShoppingListStrategyRoomDb : ShoppingListRepositoryStrategy {
    private val database = AppDatabase.instance

    override fun getAll(): LiveData<List<ShoppingListItem>> = database.shoppingListDao().all()

    override fun insert(vararg items: ShoppingListItem) = database.shoppingListDao().insert(*items)

    override fun delete(vararg items: ShoppingListItem) = database.shoppingListDao().delete(*items)

    override fun update(vararg items: ShoppingListItem) = database.shoppingListDao().update(*items)

    override fun updateOrInsert(vararg items: ShoppingListItem) = database.shoppingListDao().updateOrInsert(*items)

}

class NoOpMigration(startVersion: Int, endVersion: Int) : Migration(startVersion, endVersion) {
    override fun migrate(database: SupportSQLiteDatabase) {
        Log.i("Database", "No-Operation migration from ${this.startVersion} to ${this.endVersion}")
    }
}

class MigrationImpl(
    startVersion: Int, endVersion: Int,
    private val function: (database: SupportSQLiteDatabase) -> Unit
) : Migration(startVersion, endVersion) {
    override fun migrate(database: SupportSQLiteDatabase) {
        Log.i("Database", "Upgrading from ${this.startVersion} to ${this.endVersion}")
        this.function(database)
    }
}