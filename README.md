 # Simple shopping list/todo list
A simple app to use as a shopping list or as a todo list or any other sort of list you need. No Android permissions required, not internet connectivity required, no tracking, no bullshit.

## Usage
- Add new items to the list by clicking on the **+** button in the lower right corner.
    - Add an item name, and optionally a quantity and a more detailed description.
        - Quantities higher than 1 will be prepended to the name.
        - Description is visible by clicking on the edit button.
- Mark list items as complete by tapping on them. Undo by tapping on it again.
- Delete items from the list by swiping to the left. Currently that can't be undone.
- Change the order of list items by dragging items.
- Edit name, amount or description by clicking on the edit button.

---
## Install
Download [the .apk file](./app-release.apk), copy it to your device and install it.  
Alternatively: see the *Build it yourself* section below.

---
## Build it yourself
### Command line / Gradle
1. Clone this repo  
    `git clone https://bitbucket.org/Rhiki/shoppinglist.git`
2. Change into the source directory  
    `cd ./shoppinglist/src`
3. Run gradle  
    `./gradlew clean assembleRelease`
4. The resulting _.apk_ can be found at  
    `./app/build/outputs/apk/release/app-release-unsigned.apk`
5. Copy to device and install
### Android Studio
1. ` Build > Build Bundle(s) / APK(s) > Build APK(s)`  
![./img/build-with-android-studio.png](./img/build-with-android-studio.png)
2. The resulting APK can be found at `$projectDir/src/app/build/outputs/apk/release/app-release-unsigned.apk`
3. Copy to device and install

---
## Changelog
### Version 1.1.0 - 19. September 2020
- Add explicit button to edit items
- List items can now be dragged up and down to change their order in the list.
- List order is persisted.
### Version 1.0.0 - 18. September 2020
- Initial release